import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  username: String = "";
  password: String = "";

  constructor() { }

  ngOnInit(): void {
  }

  login(): void {
    console.log(this.username + "Login successfully");
    this.reset();
  }

  reset(): void {
    this.username= "";
    this.password= "";
  }

}
